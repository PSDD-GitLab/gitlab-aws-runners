#!/usr/bin/env bash

set -eu

if [ -z "$1" ] ; then
  echo "Please provide the Gitlab Token to register the runner"
  exit 1
fi

DeploymentAccountId=$(aws sts get-caller-identity --query Account --output text)

aws cloudformation deploy --parameter-overrides GitlabToken="$1" --template-file ci_template.yaml --stack-name gitlab-ci --capabilities CAPABILITY_IAM --no-fail-on-empty-changeset

aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin "$DeploymentAccountId".dkr.ecr.eu-central-1.amazonaws.com
docker build -t fargate-ci-runner .
docker tag fargate-ci-runner:latest "$DeploymentAccountId".dkr.ecr.eu-central-1.amazonaws.com/fargate-ci-runner:latest
docker push 268201745953.dkr.ecr.eu-central-1.amazonaws.com/fargate-ci-runner:latest
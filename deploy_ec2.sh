#!/usr/bin/env bash

set -eu

if [ -z "$1" ] ; then
  echo "Please provide the Gitlab Token to register the runner"
  exit 1
fi

aws cloudformation deploy --parameter-overrides GitlabToken="$1" --template-file ec2_template.yaml --stack-name gitlab-ci --capabilities CAPABILITY_IAM --no-fail-on-empty-changeset
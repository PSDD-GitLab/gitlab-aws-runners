AWSTemplateFormatVersion: '2010-09-09'

Parameters:
  SubnetID:
    Type: String
    Default: 'subnet-0bb143cf9538bde69'
    Description: 'ID of subnet 1'
  VpcId:
    Type: String
    Default: 'vpc-018018d4127797ca1'
    Description: 'VPC of existing VPC'
  RunnerTag:
    Type: String
    Default: 'dev-deployer'
  Region:
    Type: String
    Default: 'eu-central-1'
  EC2InstanceType:
    Type: String
    Default: 't3a.micro'
  GitlabToken:
    Type: String

Resources:
  InstanceSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Default security group
      VpcId:
        Ref: VpcId
  EC2Profile:
    Type: "AWS::IAM::InstanceProfile"
    Properties:
      Roles:
        - !Ref EC2Role
  EC2Role:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
        - "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  EC2Instance:
    Type: AWS::EC2::Instance
    Properties:
      Tags:
        - Key: Name
          Value: gitlab-ci-runner
      InstanceType: !Ref EC2InstanceType
      ImageId: "ami-0006ba1ba3732dd33"
      SubnetId: !Ref SubnetID
      SecurityGroupIds:
        - !Ref InstanceSecurityGroup
      IamInstanceProfile: !Ref EC2Profile
      UserData:
        Fn::Base64:
          !Sub |
            #!/bin/bash
            yum update
            yum install -y git curl jq yum-cron
            systemctl enable yum-cron.service
            systemctl start yum-cron.service
            sed -i '/apply_updates/c\apply_updates = yes' /etc/yum/yum-cron.conf
            (crontab -l ; echo "00 03 * * * reboot") | crontab -
            cd ~
            mkdir -p /opt/gitlab-runner/{metadata,builds,cache}
            curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm"
            rpm -i gitlab-runner_amd64.rpm
            gitlab-runner register --url https://gitlab.com/ --tag-list ${RunnerTag} --executor custom \
              --non-interactive --registration-token ${GitlabToken} --name fargate-runner
            curl -Lo /opt/gitlab-runner/fargate "https://gitlab-runner-custom-fargate-downloads.s3.amazonaws.com/latest/fargate-linux-amd64"
            chmod +x /opt/gitlab-runner/fargate
          
            curl -sSLf "$(curl -sSLf https://api.github.com/repos/tomwright/dasel/releases/latest | grep browser_download_url | grep linux_amd64 | grep -v .gz | cut -d\" -f 4)" -L -o dasel && chmod +x dasel
            mv ./dasel /usr/bin/dasel
            cd /etc/gitlab-runner/
            dasel put string -f config.toml .runners.[0].builds_dir "/opt/gitlab-runner/builds"
            dasel put string -f config.toml .runners.[0].cache_dir "/opt/gitlab-runner/cache"
            dasel put string -f config.toml .runners.[0].custom.config_exec "/opt/gitlab-runner/fargate"
            dasel put string -f config.toml .runners.[0].custom.config_args.[] -v "--config"
            dasel put string -f config.toml .runners.[0].custom.config_args.[] "/etc/gitlab-runner/fargate.toml"
            dasel put string -f config.toml .runners.[0].custom.config_args.[] "custom"
            dasel put string -f config.toml .runners.[0].custom.config_args.[] "config"
            dasel put string -f config.toml .runners.[0].custom.prepare_exec "/opt/gitlab-runner/fargate"
            dasel put string -f config.toml .runners.[0].custom.prepare_args.[] -v "--config"
            dasel put string -f config.toml .runners.[0].custom.prepare_args.[] "/etc/gitlab-runner/fargate.toml"
            dasel put string -f config.toml .runners.[0].custom.prepare_args.[] "custom"
            dasel put string -f config.toml .runners.[0].custom.prepare_args.[] "prepare"
            dasel put string -f config.toml .runners.[0].custom.run_exec "/opt/gitlab-runner/fargate"
            dasel put string -f config.toml .runners.[0].custom.run_args.[] -v "--config"
            dasel put string -f config.toml .runners.[0].custom.run_args.[] "/etc/gitlab-runner/fargate.toml"
            dasel put string -f config.toml .runners.[0].custom.run_args.[] "custom"
            dasel put string -f config.toml .runners.[0].custom.run_args.[] "run"
            dasel put string -f config.toml .runners.[0].custom.cleanup_exec "/opt/gitlab-runner/fargate"
            dasel put string -f config.toml .runners.[0].custom.cleanup_args.[] -v "--config"
            dasel put string -f config.toml .runners.[0].custom.cleanup_args.[] "/etc/gitlab-runner/fargate.toml"
            dasel put string -f config.toml .runners.[0].custom.cleanup_args.[] "custom"
            dasel put string -f config.toml .runners.[0].custom.cleanup_args.[] "cleanup"
            
            echo 'LogLevel = "info"
            LogFormat = "text"
            
            [Fargate]
              Cluster = "gitlab-ci-cluster"
              Region = "${Region}"
              Subnet = "${SubnetID}"
              SecurityGroup = "${FargateTaskSecurityGroup.GroupId}"
              TaskDefinition = "gitlab-ci-task"
              EnablePublicIP = false
            
            [TaskMetadata]
              Directory = "/opt/gitlab-runner/metadata"
            
            [SSH]
              Username = "root"
              Port = 22' > /etc/gitlab-runner/fargate.toml

  CITaskRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ecs-tasks.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/AdministratorAccess"
  ECSCluster:
    Type: 'AWS::ECS::Cluster'
    Properties:
      CapacityProviders:
        - FARGATE
        - FARGATE_SPOT
      ClusterName: gitlab-ci-cluster
      ClusterSettings:
        - Name: containerInsights
          Value: disabled
      DefaultCapacityProviderStrategy:
        - CapacityProvider: FARGATE
  ECSTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      ContainerDefinitions:
        - Name: ci-coordinator
          Image:
            !Sub "${AWS::AccountId}.dkr.ecr.eu-central-1.amazonaws.com/fargate-ci-runner:latest"
          PortMappings:
            - ContainerPort: '22'
              Protocol: tcp
          LogConfiguration:
            Options:
              awslogs-group: /ecs/topspot-ci-test-task
              awslogs-region: eu-central-1
              awslogs-stream-prefix: ecs
              awslogs-create-group: 'true'
            LogDriver: awslogs
      Family: gitlab-ci-task
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      Cpu: 2 vCPU
      Memory: 8 GB
      RuntimePlatform:
        CpuArchitecture: X86_64
        OperatingSystemFamily: LINUX
      ExecutionRoleArn: !GetAtt CITaskRole.Arn
      TaskRoleArn: !GetAtt CITaskRole.Arn
  FargateTaskSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: "SSH allowing security group. Will be referenced in gitlab runner config files"
      VpcId:
        Ref: VpcId
  FargateTaskSGIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref FargateTaskSecurityGroup
      IpProtocol: tcp
      FromPort: 22
      ToPort: 22
      CidrIp: !Sub "${EC2Instance.PrivateIp}/32"
  DockerRepository:
    Type: AWS::ECR::Repository
    Properties:
      RepositoryName: "fargate-ci-runner"